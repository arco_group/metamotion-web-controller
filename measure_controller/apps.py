from django.apps import AppConfig


class MeasureControllerConfig(AppConfig):
    name = 'measure_controller'
